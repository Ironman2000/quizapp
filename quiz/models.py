from django.db import models


class Quiz(models.Model):
	title = models.CharField(max_length=300, blank=False)
	category = models.CharField(max_length=70, blank=False)

	def __str__(self):
		return self.title
